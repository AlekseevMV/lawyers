﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using lawyers.Models;
using System.Threading;

namespace lawyers.Controllers
{
    public class FirmsController : Controller
    {
        private ParserContext db = new ParserContext();

        public ActionResult GetStates()
        {
            var st = from s in db.states
                     //join c in db.Cities on s.id equals c.state.id
                     //into Cities
                     select new { s.id, s.Name, CitiesCount = s.Cities.Count() };

            return Json(st.ToList(),JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCities(int StateId)
        {
            var st = db.states.Find(StateId);
            if(st==null){
                return Json("st==null",JsonRequestBehavior.AllowGet);
            }

            var cities = st.Cities;
            var res = from c in cities
                      select new { c.id, c.Name, AreasCount = c.Areas.Count() };
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAreas(int CityId)
        {
            var c = db.Cities.Find(CityId);
            if (c == null)
            {
                return Json("city==null", JsonRequestBehavior.AllowGet);
            }
            var areas = c.Areas;
            var res = from a in areas
                      select new { a.id, a.Name, FirmsCount = a.Firms.Count() };
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFirms(int AreaId)
        {
            var a = db.Areas.Find(AreaId);
            if (a == null)
            {
                return Json("area==null", JsonRequestBehavior.AllowGet);
            }
            var firms = a.Firms;
            var res = from f in firms
                      select new { f.id, f.Name, Parsed = (f.Address != null) };
            var listres = res.ToList();
            return Json(listres, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveStates(IEnumerable<state> states)
        {
            foreach (var state in states)
            {
                if (db.states.Where(s => s.Name == state.Name).Count() == 0)
                {
                    db.states.Add(state);
                }
            }
            db.SaveChanges();
            return Json("Ok");
        }

        [HttpPost]
        public ActionResult SaveCities(IEnumerable<City> Cities, int StateId )
        {
            var State = db.states.Find(StateId);
            foreach (var city in Cities)
            {
                if (State.Cities.Where(c => c.Name == city.Name).Count() == 0)
                {
                    State.Cities.Add(city);
                }
            }
            db.SaveChanges();
            return Json("Ok");
        }

        public ActionResult SaveAreas(IEnumerable<Area> Areas, int CityId)
        {
            var City = db.Cities.Find(CityId);
            foreach (var Area in Areas)
            {
                if (City.Areas.Where(c => c.Name == Area.Name).Count() == 0)
                {
                    City.Areas.Add(Area);
                }
            }
            db.SaveChanges();
            return Json("Ok");
        }

        public ActionResult SaveFirms(IEnumerable<Firm> Firms, int AreaId)
        {
            var Area = db.Areas.Find(AreaId);
            var City = Area.City;
            
            foreach (var f in Firms)
            {
                Firm firm = City.Firms.Where(c => c.Name == f.Name).FirstOrDefault();
                if (firm == null)
                {
                    //если фирмы с таким названием нет в фирмах города
                    City.Firms.Add(f);  //сохраняем фирму в город
                    Area.Firms.Add(f);  //сохраняем фирму в Area
                }
                else
                {
                    if (Area.Firms.Where(c => c.Name == f.Name).Count() == 0)
                    {
                        //Если такая фирма уже есть в городе, но нет в Area
                        Area.Firms.Add(firm); //сохраняем ее в Area
                    }
                }
            }
            db.SaveChanges();
            return Json("Ok");
        }

        public ActionResult SaveFirmDetails(Firm FirmDetails, int FirmId)
        {
            var Firm = db.Firms.Find(FirmId);
            if (Firm == null)
            {
                return Json("FirmId==" + FirmId + " not found");
            }
            Firm.Address = FirmDetails.Address;
            Firm.AreasOfLaw = FirmDetails.AreasOfLaw;
            Firm.FirmSize = FirmDetails.FirmSize;
            Firm.People = FirmDetails.People;
            Firm.Phone = FirmDetails.Phone;
            Firm.Site = FirmDetails.Site;
            Firm.YearEstablished = FirmDetails.YearEstablished;
            db.Entry(Firm).State = EntityState.Modified;
            db.SaveChanges();
            return Json("Ok");
        }


        public ActionResult downloadCitiesPage(int StateId)
        {
            var State = db.states.Find(StateId);
            var res = Client.Get(State.Url);
            return Content(res);
        }

        public ActionResult downloadAreasPage(int CityId)
        {
            var City = db.Cities.Find(CityId);
            var res = Client.Get(City.Url);
            return Content(res);
        }

        public ActionResult downloadFirmsPage(int AreaId)
        {
            var Area = db.Areas.Find(AreaId);
            var res = Client.Get(Area.Url);
            return Content(res);
        }

        public ActionResult downloadFirmDetailsPage(int FirmId)
        {
            var Firm = db.Firms.Find(FirmId);
            var res = Client.Get(Firm.Url);
            return Content(res);
        }

        public ActionResult TestThreads(int i)
        {
            Thread.Sleep(2000);
            return Content(i.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
