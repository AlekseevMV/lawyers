﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace lawyers.Controllers
{
    public static class Client
    {
        public static string Get(string url)
        {
            string result;
            using (WebClient client = new WebClient())
            {
                result = client.DownloadString(url + "&hl=e1");
            }
            return result;
        }
    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }

        public ActionResult getUrl(string url)
        {
            string result;
            result = Client.Get(url);
            /*if (result.Length < 2000)
            {
                return HttpNotFound();
            }*/
            return Content(result);
        }

        public ActionResult scrape()
        {
            return View();
        }
    }
}
