namespace lawyers.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        City_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Cities", t => t.City_id, cascadeDelete: true)
                .Index(t => t.City_id);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        state_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.states", t => t.state_id, cascadeDelete: true)
                .Index(t => t.state_id);
            
            CreateTable(
                "dbo.Firms",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        Site = c.String(),
                        YearEstablished = c.String(),
                        FirmSize = c.String(),
                        City_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Cities", t => t.City_id)
                .Index(t => t.City_id);
            
            CreateTable(
                "dbo.states",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.FirmAreas",
                c => new
                    {
                        Firm_id = c.Int(nullable: false),
                        Area_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Firm_id, t.Area_id })
                .ForeignKey("dbo.Firms", t => t.Firm_id, cascadeDelete: true)
                .ForeignKey("dbo.Areas", t => t.Area_id, cascadeDelete: true)
                .Index(t => t.Firm_id)
                .Index(t => t.Area_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Areas", "City_id", "dbo.Cities");
            DropForeignKey("dbo.Cities", "state_id", "dbo.states");
            DropForeignKey("dbo.Firms", "City_id", "dbo.Cities");
            DropForeignKey("dbo.FirmAreas", "Area_id", "dbo.Areas");
            DropForeignKey("dbo.FirmAreas", "Firm_id", "dbo.Firms");
            DropIndex("dbo.FirmAreas", new[] { "Area_id" });
            DropIndex("dbo.FirmAreas", new[] { "Firm_id" });
            DropIndex("dbo.Firms", new[] { "City_id" });
            DropIndex("dbo.Cities", new[] { "state_id" });
            DropIndex("dbo.Areas", new[] { "City_id" });
            DropTable("dbo.FirmAreas");
            DropTable("dbo.states");
            DropTable("dbo.Firms");
            DropTable("dbo.Cities");
            DropTable("dbo.Areas");
        }
    }
}
