namespace lawyers.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArraysToStrings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Firms", "People", c => c.String());
            AddColumn("dbo.Firms", "AreasOfLaw", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Firms", "AreasOfLaw");
            DropColumn("dbo.Firms", "People");
        }
    }
}
