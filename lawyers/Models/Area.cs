﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lawyers.Models
{
    public class Area
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<Firm> Firms { get; set; }
    }
}