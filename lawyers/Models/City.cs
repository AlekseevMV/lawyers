﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lawyers.Models
{
    public class City
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual state state {get;set;}
        public virtual ICollection<Area> Areas { get; set; }
        public virtual ICollection<Firm> Firms { get; set; }
    }
}