﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lawyers.Models
{
    public class Firm
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Site { get; set; }
        public string YearEstablished { get; set; }
        public string FirmSize { get; set; }
        public string People { get; set; }
        public string AreasOfLaw { get; set; }
        public virtual ICollection<Area> Areas { get; set; }
        public virtual City City { get; set; }
    }
}