﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace lawyers.Models
{
    public class ParserContext: DbContext
    {
        public virtual DbSet<Firm> Firms { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<state> states { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Firm>()
                .HasRequired(e => e.City)
                .WithMany(e => e.Firms)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Firm>()
                .HasMany(e => e.Areas)
                .WithMany(e => e.Firms);
            modelBuilder.Entity<Area>()
                .HasRequired(e => e.City)
                .WithMany(e => e.Areas)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<City>()
                .HasRequired(e => e.state)
                .WithMany(e => e.Cities)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<state>()
                .HasMany(e => e.Cities);
        }
    }
}