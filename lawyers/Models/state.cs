﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lawyers.Models
{
    public class state
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual ICollection<City> Cities { get; set; }
    }
}