﻿$(document).ready(function () {
    states = Array();
    firms = Array();
    var maxThreads = 10;
    getStates();
    $('#progress').hide();
    $('#start').click(parserGo);
    $('#ParseEmpty').click(parseEmpty);
    bindClicks();

    function bindClicks() {
        $('.parseCities').unbind('click', clickParseCities);
        $('.parseAreas').unbind('click', clickParseAreas);
        $('.parseFirms').unbind('click', clickParseFirms);
        $('a.State').unbind('click', clickShowCities);
        $('a.City').unbind('click', clickShowAreas);
        $('a.Area').unbind('click', clickShowFirms);
        $('.parseFirmDetails').unbind('click', clickParseFirmDetails);
        
        $('.parseCities').click(clickParseCities);
        $('.parseAreas').click(clickParseAreas);
        $('.parseFirms').click(clickParseFirms);
        $('a.State').click(clickShowCities);
        $('a.City').click(clickShowAreas);
        $('a.Area').click(clickShowFirms);
        $('.parseFirmDetails').click(clickParseFirmDetails);
    }

    function clickParseCities() {
        var StateId = $(this).attr('StateId');
        setActiveState(StateId);
        var c = $.ajax({
            url: '/Firms/DownloadCitiesPage',
            data: { Stateid: StateId }
        });
        c.done(function (d) {
            var Cities = parseCity(d);
            saveCities(Cities, StateId);
        });
        c.fail(function (e, g, f) {
            alert("Ошибка при загрузке страницы со списком городов");
        })
        return false;
    }

    function clickParseAreas() {
        var CityId = $(this).attr('CityId');
        setActiveCity(CityId);
        var a = $.ajax({
            url: '/Firms/DownloadAreasPage',
            data: { CityId: CityId }
        });
        a.done(function (d) {
            var Areas = parseAreas(d);
            saveAreas(Areas, CityId);
        });
        a.fail(function (e, g, f) {
            alert("Ошибка при загрузке страницы со списком Areas of law");
        })
        return false;
    }

    function clickParseFirms() {
        var AreaId = $(this).attr('AreaId');
        setActiveArea(AreaId);
        var a = $.ajax({
            url: '/Firms/DownloadFirmsPage',
            data: { AreaId: AreaId }
        });
        a.done(function (d) {
            parseFirms(d, AreaId);
        });
        a.fail(function (e, g, f) {
            alert("Ошибка при загрузке страницы со списком фирм");
        })
        return false;
    }

    function clickParseFirmDetails() {
        var FirmId = $(this).attr('FirmId');
        setActiveFirm(FirmId);
        var a = $.ajax({
            url: '/Firms/DownloadFirmDetailsPage',
            data: { FirmId: FirmId }
        });
        a.done(function (d) {
            var details = parseFirmDetails(d);
            saveFirmDetails(details, FirmId);
        });
        a.fail(function (e, g, f) {
            alert("Ошибка при загрузке детальной информации о фирме");
        })
    }

    function setActiveState(id) {
        setActive(id, "State");
    }

    function setActiveCity(id) {
        setActive(id, "City");
    }

    function setActiveArea(id) {
        setActive(id, "Area");
    }

    function setActiveFirm(id) {
        setActive(id, "Firm");
    }

    function setActive(id, type) {
        $('a.' + type + '.active').removeClass('active');
        $('a[' + type + 'Id="' + id + '"]').addClass("active");
    }

    function clickShowCities() {
        clickShow(this, 'State');
        return false;
    }

    function clickShowAreas() {
        clickShow(this, 'City');
        return false;
    }

    function clickShowFirms() {
        clickShow(this, 'Area');
        return false;
    }

    function clickShow(selector, type) {
        var Id = $(selector).attr(type + 'Id');
        setActive(Id, type);
        getEntity(Id, type);
        return false;
    }

    function getEntity(id, type) {
        switch (type) {
            case 'State': getCities(id);
                break;
            case 'City': getAreas(id);
                break;
            case 'Area': getFirms(id);
                break;
        }
    }

    function getStates() {
        var s = $.ajax({
            url: '/Firms/GetStates'
        });
        s.done(function (d) {
            states = d;
            repaintStates();
        })
    }

    function getCities(stateId) {
        var s = $.ajax({
            url: '/Firms/GetCities',
            data: { StateId: stateId }
        });
        s.done(function (d) {
            repaintCities(d);
            repaintCitiesCount(d.length, stateId);
        })
    }

    function getAreas(CityId) {
        var a = $.ajax({
            url: '/Firms/GetAreas',
            data: { CityId: CityId }
        });
        a.done(function (d) {
            repaintAreas(d);
            repaintAreasCount(d.length, CityId);
        })
    }

    function getFirms(AreaId) {
        var f = $.ajax({
            url: '/Firms/GetFirms',
            data: { AreaId: AreaId }
        });
        f.done(function (d) {
            repaintFirms(d);
            repaintFirmsCount(d.length, AreaId);
        })
    }

    function repaintStates() {
        var res = '<div class="list-group">';
        states.forEach(function (state) {
            res += '<a href="#" class="list-group-item State" StateId="' + state.id + '">';
            res += state.Name;
            res += '<span class="badge"><input class="parseCities" StateId="' + state.id + '" type="button" value="P"></span>';
            res += '<span class="badge CitiesCount" StateId="' + state.id + '">' + state.CitiesCount + '</span>';
            res += '</a>';
        });
        res += '</div>';
        $('#states').html(res);
        bindClicks();
    }

    function repaintCities(cities) {
        var res = '<div class="list-group">';
        cities.forEach(function (city) {
            res += '<a href="#" class="list-group-item City" CityId="' + city.id + '">';
            res += city.Name;
            res += '<span class="badge"><input class="parseAreas" CityId="' + city.id + '" type="button" value="P"></span>';
            res += '<span class="badge AreasCount" CityId="' + city.id + '">' + city.AreasCount + '</span>';
            res += '</a>';
        });
        res += '</div>';
        $('#cities').html(res);

        bindClicks();
    }

    function repaintAreas(areas) {
        var res = '<div class="list-group">';
        areas.forEach(function (area) {
            res += '<a href="#" class="list-group-item Area" AreaId="' + area.id + '">';
            res += area.Name;
            res += '<span class="badge"><input class="parseFirms" AreaId="' + area.id + '" type="button" value="P"></span>';
            res += '<span class="badge FirmsCount" AreaId="' + area.id + '">' + area.FirmsCount + '</span>';
            res += '</a>';
        });
        res += '</div>';
        $('#areas').html(res);
        bindClicks();
    }

    function repaintFirms(firms) {
        var res = '<div class="list-group">';
        firms.forEach(function (firm) {
            res += '<a href="#" class="list-group-item Firm" FirmId="' + firm.id + '">';
            res += firm.Name;
            if (!firm.Parsed) {
                res += '<span class="badge"><input class="parseFirmDetails" FirmId="' + firm.id + '" type="button" value="P"></span>';
            } else {
                res += '<span class="badge"><input class="showFirm" FirmId="' + firm.id + '" type="button" value="S"></span>';
            }
            res += '</a>';
        });
        res += '</div>';
        $('#firms').html(res);
        bindClicks();
    }

    function setFirmDetailsParsed(firmid) {
        var f = $('input[FirmId="' + firmid + '"]');
        f.removeClass('parseFirmDetails');
        f.attr('value', 'S');
    }

    function repaintCitiesCount(count, id) {
        $('.CitiesCount[StateId="' + id + '"]').text(count);
    }

    function repaintAreasCount(count, id) {
        $('.AreasCount[CityId="' + id + '"]').text(count);
    }

    function repaintFirmsCount(count, id) {
        $('.FirmsCount[AreaId="' + id + '"]').text(count);
    }

    function ajaxStart() {
        $('#progress').show();
    }
    function ajaxStop() {
        $('#progress').hide();
    }

    function parseEmpty() {
        return false;
    }

    function parserGo() {
        ajaxStart();
        states = Array();
        var b = $.ajax({
            url: '/home/getUrl',
            data: { url: 'http://service-proxy.com/index.php?q=http%3A%2F%2Fwww.lawyers.com%2Ffind-law-firms-by-location%2F' }
        });
        b.done(function (d) {
            parseStates(d);
            return;
            startParseCities();
        });
        b.fail(function (e, g, f) {
            alert('Epic Fail');
        });
    }

    function parseStates(data) {
        var res = '<ul>';
        $(data).find('.states li a').each(function (i, elem) {
            var n = $(this).text();
            var h = $(this).attr('href');
            res += '<li id="' + i + '">' + n + '</li>';
            states.push({
                Name: n,
                Url: h
            });
        })
        res += '<ul>';
        $('#states').html(res);
        saveStates();
    }

    function parseFirmDetails(data) {
        /*public int id { get; set; }
          public string Name { get; set; }
          public string Url { get; set; }
          public string[] People { get; set; }
          public string Address { get; set; }
          public string Phone { get; set; }
          public string Site { get; set; }
          public string YearEstablished { get; set; }
          public string FirmSize { get; set; }
          public string[] AreasOfLaw { get; set; }*/
        var p = Array();
        //$(data).find('#peopleAtThisFirmWidget li[ng-repeat="attorney in getAttorneys()"] strong a').each(function () {
        //    people.push($(this).attr('title'));
        //})
        var people = p.toString();
        var addr = Array();
        $(data).find('span[itemprop="address"]').first().find('span').each(function () {
            addr.push($(this).text());
        })
        var address = addr.toString();
        var phones = Array();
        $(data).find('div:not(.profile-phone)>span[itemprop="telephone"]').each(function () {
            phones.push($(this).text());
        });
        var phone = phones.toString();
        var Site = 'http://' + $(data).find('div.profile-website a').first().text();
        var YearEstablished = $(data).find('span[itemprop = "foundingDate"]').first().text();
        var FirmSize = $(data).find('div:contains("Firm Size"):last').next().text().trim();
        //.next().first().text();
        var a = Array();
        $(data).find('div#areasOfLaw li').each(function () {
            a.push($(this).text());
        });
        var AreasOfLaw = a.toString();
        var firm = {
            Name: '',
            Url: '',
            People: people,
            Address: address,
            Phone: phone,
            Site: Site,
            YearEstablished: YearEstablished,
            FirmSize: FirmSize,
            AreasOfLaw: AreasOfLaw
        };
        return firm;
    }

    function saveStates() {
        var b = $.ajax({
            type: "POST",
            url: '/Firms/SaveStates',
            data: {
                states: states
            }
        });
        b.done(function (d) {
            //alert("Сохранение штатов - " + d);
        });
        b.fail(function (e, g, f) {
            alert("Ошибка сохранения штатов");
        })
    };

    function saveCities(c, sid) {
        var b = $.ajax({
            type: "POST",
            url: '/Firms/SaveCities',
            data: {
                StateId: sid,
                Cities: c
            }
        });
        b.done(function (d) {
            //alert("Сохранение городов - " + d);
            getCities(sid);
        });
        b.fail(function (e, g, f) {
            alert("Ошибка сохранения городов");
        })
    }

    function saveAreas(a, cid) {
        var b = $.ajax({
            type: "POST",
            url: '/Firms/SaveAreas',
            data: {
                CityId: cid,
                Areas: a
            }
        });
        b.done(function (d) {
            //alert("Сохранение Areas of law - " + d);
            getAreas(cid);
        });
        b.fail(function (e, g, f) {
            alert("Ошибка сохранения Areas of law");
        })
    }

    function saveFirms(f, aid) {
        var b = $.ajax({
            type: "POST",
            url: '/Firms/SaveFirms',
            data: {
                AreaId: aid,
                Firms: f
            }
        });
        b.done(function (d) {
            //alert("Сохранение Firms - " + d);
            getFirms(aid);
        });
        b.fail(function (e, g, f) {
            alert("Ошибка сохранения Firms");
        })
    }

    function saveFirmDetails(details, FirmId) {
        var b = $.ajax({
            type: "POST",
            url: '/Firms/SaveFirmDetails',
            data: {
                FirmId: FirmId,
                FirmDetails: details
            }
        });
        b.done(function (d) {
            setFirmDetailsParsed(FirmId);
        });
        b.fail(function (e, g, f) {
            alert("Ошибка сохранения детальных данных фирмы");
        });
    };

    var currentState = 0;
    var threads = 0;

    function startParseCities() {
        currentState = 0;
        threads = 0;
        releaseThread();
    }

    function releaseThread() {
        while ((threads < maxThreads) && (currentState < states.length)) {
            threads++;
            parseCities(states[currentState++], currentState - 1);
        }
        if ((currentState >= states.length) && (threads == 0)) {
            ajaxStop();
        }
    }

    function parseCities(state, ind) {
        state.cities = Array();
        var s = $.ajax({
            url: '/home/getUrl',
            data: { url: state.Url }
        });
        s.done(function (data) {
            state.cities = parseCity(data);
            $("#" + ind).append(" - " + state.cities.length + " городов");
            threads--;
            releaseThread();
        })
        s.fail(function (e, g, f) {
            alert("Штат " + state.Name + " - ошибка при парсинге городов");
            threads--;
            releaseThread();
        })
    };

    function parseCity(page) {
        var cities = new Array();
        $(page).find('.content .active .states li a').each(function () {
            var n = $(this).text();
            var h = $(this).attr('href');
            cities.push({
                name: n,
                url: h
            });
        });
        return cities;
    }

    function parseAreas(page) {
        var areas = new Array();
        $(page).find('.content.active .states li a').each(function () {
            var n = $(this).text();
            var h = $(this).attr('href');
            areas.push({
                name: n,
                url: h
            });
        });
        return areas;
    }

    function parseFirms(page, AreaId) {
        firms = new Array();
        parseLoop(page, AreaId);
    }

    function parseLoop(page, AreaId) {
        parseFirmsPage(page);
        var url = $(page).find('.pagination_next a').attr('href');
        if (url != null) {//если есть ссылка на следующую страницу,
            //скачиваем следующую страницу
            var b = $.ajax({
                url: '/Home/getUrl',
                data: { url: url }
            });
            b.done(function (page) {
                //если страница скачалась, запускаем парсер заново
                parseLoop(page, AreaId);
            });
            b.fail(function (page) {
                alert("Ошибка при скачивании следующей станицы фирм");
            });
        } else {
            //Если все страницы скачали, запускаем сохранение фирм
            saveFirms(firms, AreaId);
        }
    }

    function parseFirmsPage(p) {
        //alert("Парсим страницу с фирмами");
        $(p).find('.displayname a').each(function () {
            var n = $(this).text();
            var url = $(this).attr('href');
            firms.push({
                Name: n,
                Url: url,
                People: '',
                Address: '',
                Phone: '',
                Site: '',
                YearEstablished: '',
                FirmSize: '',
                AreasOfLaw: ''
            })
        })
    }

    //объект для ограничения количества одновременных запросов
    //getData - поместить запрос в формате $.ajax в очередь.
    ajaxThreads = {
        stack: [],
        maxThreads: 6,
        currentThreads: 0,

        getData: function (o, done, fail) {
            this.stack.push({ o: o, done: done, fail: fail });
            this.loop();
        },

        loop: function F() {
            if (this.currentThreads < this.maxThreads) {
                var req = this.stack.pop();
                if (req != null) {
                    var _this = this;
                    this.currentThreads++;
                    var a = $.ajax(req.o);
                    if (req.done != null) {
                        a.done(req.done);
                    }
                    if (req.fail != null) {
                        a.fail(req.fail);
                    }
                    a.complete((function (context) {
                        return function () {
                            context.currentThreads--;
                            context.loop();
                        }
                    })(this));
                }
            }
        }
    }

});
